## Questions / Remarques / suggestions 

1.2.1 
Définition de variable aléatoire
* qu'est-ce qu'une expérience aléatoire ? Est-ce finalement juste la donnée d'un univers ? Où est-on : 
	* dans les maths, si oui, de quel objet s'agit-il ?	
	* dans l'interprétation des maths et dans ce cas, le concept peut-il entrer dans une définition ?
	* Dans le monde réel physique ?
	* Au lycée, la notion apparaît dans pas mal de bouquins, mais personne ne la définit rigoureusement, j'ai donc eu l'impression qu'étant à cheval entre la physique et la modélisation mathématique de la physique, on peut s'en dispenser et se contenter d'univers, d'ensemble de parties mesurables et de loi de probabilité dont je peux définir les types mathématiques proprement.
* Une variable aléatoire semble être définie comme simple fonction d'ensemble de départ l'univers. Y a-t-il une restriction sur les fonctions acceptables, que ce soit pour l'ensemble de  d'arrivée, ou des propriétés de la correspondance ?

1.2.2 
*  Notation très détaillée sur la somme, moins sur celle X $\in$ A, j'ignore les prérequis des personnes auxquelles s'adressent le cours, mais peut-être qu'il y a une inhomogénéité dans le détail des notations, après la définition de X comme "règle". 
*  Idem : préciser $\mathbb{P}(X =x_i, Y=yi)$ est égal à $\mathbb{P}( \{X=x_i\} \cap \{Y=yi\} )$ ?

1.3.1 
* $Y(\omega)$ = 0 d'après la définition de $Y$ pour les grandes tailles (et manque le "0 sinon", pour la taille)

1.3.3 : 
* je suis une bille en probas et en stats, donc ma remarque va sans doute paraître un peu stupide. Il m'a étonné en revoyant la notion d'espérance conditionnelle que l'objet en question soit une V.A alors que l'espérance n'est pas définie comme telle (mais simplement comme un réel). Alors que dans le même temps une probabilité et une probabilité conditionnelle sont des objets de même type.
* Evidemment, ne connaissant pas bien cette notion, il m'a surpris et intéressé qu'on définisse la notion de probabilité conditionnelle à partir de la notion d'espérance (conditionnelle), alors qu'au lycée évidemment, on ne peut pas faire ainsi.
* Si ϕ est une fonction quelconque, alors $\mathbb{E}[ϕ(X)Y∣X]=ϕ(X)\mathbb{E}[Y∣X]$. 
Y a-t-il une interprétation simple de cette propriété à l'instar de celles que tu proposes pour les autres ?

1.3.6 
* Le tirage des $n$ individus s'effectue avec remise, j'imagine, puisque les variables $X_i$ sont supposées indépendantes. Ceci ne paraît évident quand au début il est mentionné : tirage aléatoire d'un échantillon (qui donne l'idée d'une partie de la population, donc d'individus différents)

1.4.2
* Ainsi, la matrice de variance-covariance est inversible si et seulement si les composantes de X et la variable aléatoire constante **égale à 1**: préciser que 1 en dimension d est le vecteur (1, 1, 1...,1) ?

2.8
* note 11 : est-ce que ce n'est finalement pas la figure 2.9 de droite qui illustre le mieux la note 11, avec une variance verticale des résidus potentiellement très faible par rapport à la valeur prédite par la courbe rouge, mais qui serait plus grande si l'on ajustait de manière affine ? (ou bien peut-être que je comprends mal la raison évoquée dans la note 11)

3.1 
* Merci d'avoir écrit quelque chose d'aussi clair !

3.2.3.3
* On imagine que les quantités achetées sont-elles aussi continues. Comment peut-on faire lorsqu'elles ne le sont pas, typiquement lorsqu'elles sont entières ? (Peut-être la suite répond à la question)

## Coquilles
1.2 : remarque : 
* discute **par**
* **(tr)availler**
* _nécessairement pas_ ou _pas nécessairement_ ?
*  leurs espaces d’arrivée respectifs (pluriel suppose plusieurs espaces d'arrivée pour une même va ?)
1.3.1  : 
* inférieures ou égales **à** x
* (juste avant le à retenir) x est supérieur**e**

1.3.4
* $V(X)=E[X]\{1−E[X]\}$ : plutôt des parenthèses qu'une accolade ?

1.3-6
* Lorsque n **tends**
* $\sqrt{\frac{n}{V(X)}}\{Sn−\mathbb{E}[X]\}$  : accolades ou parenthèses ? Ou bien, peut-être y a-t-il une signification, un usage aux accolades dans le domaines des proba stats avec lequel je ne suis pas familier. (même remarque dans le paragraphe multivarié)
* Le graphique illustrant la loi normale est joli. On ne perçoit le resserrement que lorsque l'on fait attention à l'échelle des abscisses de celui de droite.

1.4.1
* L'espérance d'une variable multidimentionnelle**s**

1.4.2
*  Ces considérations permettent **de définir la ? d’une** variable aléatoire de dimension d en étendant la définition donnée dans le cas unidimensionnel.
*  Cet écart **peut-être** mesuré en norme euclidienne (trait d'union) 
*  $\sqrt{\sum_{i=1}^d\{X_i−\mathbb{E}[X_i]\}^2}$ : accolades / parenthèses ?

2.6.2.2
* Xd n'est pas définie (Xs ?) 

2.7 
* A retenir : moindre**s** carrés

2.8.1
* En effet, comme C(Xi,ϵ) **= 0** ?
* 14.6% de la variance du salaire horaire **correspond par**

2.8.3
* Coeffiicients
* Légende de la figure 2.9 : 1ere phrase : manque le verbe. 

2.9
* Des auteurs importants **recommandents**

2.9.1
* Matrice X : La première ligne se finit par $X_{i}^{d}$ au lieu de $X_{1}^{d}$
* On peut faire de même en multipliant Y à gauche **par ?** X′
*  $\frac{1}{n}\sum_{i=1}^{n} X_{j}^{k}X_{i}^{l}$ : Je ne comprends pas l'indice $j$. Ce n'est pas $i$  aussi?
* s’approche de $\mathbb{E}[X_{k}X_{l}]$ : pareil, pas sûr de comprendre l'opération entre $X_k$ et $X_l$: il ne manque pas un prime ?
* la variable aléatoire $\frac{1}{n}X^{'}X$ s’identifie dans la limite d’un très grand nombre de tirages à la matrice $\mathbb{E}[XX^{'}]$ : Est-ce bien $\mathbb{E}[XX^{'}]$ ou $\mathbb{E}[X^{'}X]$ ? J'ai l'impression que je comprends mal les dimensions. Dans mon esprit, E[X] a même dimensions que X mais X'X et XX' n'ont pas les mêmes dimensions, l'une d'entre elle étant d'une taille qui dépend du nombre $n$ d'observations. Où fais-je erreur ?

3.1.2
* Il reste un bout de commentaire au dessus de la figure de Piketty

3.2
*  l’inférence **causale.Ce** formalisme 

3.2.3.3
* la fonction qui à chaque valeur potentielle du prix p la quantité qi(p) : **manque le verbe associe**

3.2.5
* $ATT=\mathbb{E}[Y_i(1)-Y_i(0) \mid D_i=0]$ : c'est ATU = ...
* puisque l’on a toujours ATE=0, **ATT=1** et **ATT=−1**. : Pourquoi le premier ATT n'est pas égal à 3 (et le deuxième doit être ATU = -1)? 

3.2.6
* $\alpha _{3}$ vaut -2 dans le tableau modifié avec l'agriculteur omniscient

3.2.7
* En effet, **dans ce cas ne dépend pas** des valeurs potentielles des rendements de chaque parcelle : manque le sujet (le choix)
* Remarque : ... ou la possibilité d’en généraliser: ("ni de" la possibilité")

4.2.1
* Dans l’exemple choisi, on peut considérer que l’intervention qui concerne chaque candidature **vaut** 1 si cette candidature

## Avertissements 
Ils sont tous forts bienvenus 


